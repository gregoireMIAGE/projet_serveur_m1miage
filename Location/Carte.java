/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.Location;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;

/**
 *
 * @author greg
 */
public class Carte {

    protected String name;
    protected String location;//adresse image 1
    protected int taille;
    protected double x;
    protected double y;
    protected Group group;

    public Carte(String name, String location) {
        this.name = name;
        this.location = location;

    }

    protected Carte(String name, ImageView imageView) {
        this.name = name;
    }

    public String getLocation() {
        return this.location;
    }

    public String getName() {
        return this.getName();
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

}

class CarteRail extends Carte {

    /*ATTRIBUT*/
    //permet de connaitre les point de ratachement du rail
    private int pointAttach[];
    private int positionXmap;
    private int positionYmap;
    private int place;//place de la carte dans la bar
    private boolean validate;//permet de definir si la carte a teait validé
    private String locationChemin;//permet de recuperer l'imagepour tracer un chemin 
    private ArrayList<Chemin> lesChemins; //represente le ou les chemin au quel appartien le rail (le père)

    /*CONSTRUCTEUR*/
    public CarteRail(String name, String location, int place, int pointAttach[]) {
        super(name, location);
        this.lesChemins = new ArrayList<Chemin>();
        this.pointAttach = pointAttach;
        this.validate = false;
        this.place = place;
    }

    public CarteRail(String name, String location, int place, int pointAttach[], String locationChemin) {
        super(name, location);
        this.lesChemins = new ArrayList<Chemin>();
        this.pointAttach = pointAttach;
        this.validate = false;
        this.place = place;
        this.locationChemin = locationChemin;
    }

    /*ACCESSEUR*/
    public String getNameRail() {
        return name;
    }

    public boolean getValidate() {
        return this.validate;
    }

    public void validate() {
        this.validate = true;
    }

    public void unvalidate() {
        this.validate = false;
    }

    public void setPositionXmap(int x) {
        this.positionXmap = x;
    }

    public void setPositionYmap(int Y) {
        this.positionYmap = Y;
    }

    public int getPositionXmap() {
        return this.positionXmap;
    }

    public int getPositionYmap() {
        return this.positionYmap;
    }

    public int[] getPointAttach() {
        return this.pointAttach;
    }

    public int getPointAttach(int i) {
        return this.pointAttach[i];
    }

    public void addChemin(Chemin unChemin) {
        this.lesChemins.add(unChemin);
    }

    public void addCheminAll(ArrayList<Chemin> unChemin) {
        this.lesChemins.addAll(unChemin);
    }

    public String getLocationChemin() {
        return this.locationChemin;
    }

    public Chemin getChemin(int index) {
        return this.lesChemins.get(index);
    }

    public ArrayList<Chemin> getCheminAll() {
        return this.lesChemins;
    }

    public int getLesCheminsSize() {
        return this.lesChemins.size();
    }

    /*METHODE*/
    //permet de dessiner le chemin de la carte
    public Image getImageDessineChemin() {
        return new Image(this.locationChemin);
    }

    //permet de decaler le tableau 
    public void moveToArray(String cmd) {
        int[] tabreturn = new int[this.pointAttach.length];
        if (cmd.equalsIgnoreCase("gauche")) {
            for (int i = this.pointAttach.length - 1; i > 0; i--) {
                tabreturn[i - 1] = this.pointAttach[i];
            }
            tabreturn[this.pointAttach.length - 1] = this.pointAttach[0];
        }
        if (cmd.equalsIgnoreCase("droite")) {
            for (int i = 0; i < this.pointAttach.length - 1; i++) {
                tabreturn[i + 1] = this.pointAttach[i];
            }
            tabreturn[0] = this.pointAttach[this.pointAttach.length - 1];
        }
        this.pointAttach = tabreturn;
    }

    public boolean isEqualPosition(int x, int y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionXmap == x && this.positionYmap == y);
    }

    //permet la comparaison des point d'attach entre deux carte. carte.this prioritaire donc peux etre egale a 1 alors que l'autre carte = 0
    public boolean comparePointAttach(int[] tab) {
        boolean valide = false;
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == 1 && this.pointAttach[i] == 1) {
                valide = true;
            }
        }
        return valide;
    }
}

class CarteVille extends Carte {

    public int niveau;
    public int positionXmap;
    public int positionYmap;

    public CarteVille(String name, String location, int niveau, int positionX, int positionY) {
        super(name, location);
        this.niveau = niveau;
        this.positionXmap = positionX;
        this.positionYmap = positionY;
    }

    public int getNiveau() {
        return this.niveau;
    }

    public int getPositionXMap() {
        return this.positionXmap;
    }

    public int getPositionYMap() {
        return this.positionYmap;
    }

    public boolean isEqualPosition(int x, int y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionXmap == x && this.positionYmap == y);
    }
}

class CarteMap extends Carte {

    public int positionX;
    public int positionY;
    public int construct;

    public CarteMap(String name, ImageView imageView, int positionX, int positionY, int construct) {
        super(name, imageView);
        this.positionX = positionX;
        this.positionY = positionY;
        this.construct = construct;
    }

    public int getPositionX() {
        return this.positionX;
    }

    public int getPositionY() {
        return this.positionY;
    }

    public int getConstruct() {
        return this.construct;
    }

    public boolean isEqualPosition(int x, int y) {
        //permet de retouerner si la postion correspond a la carte
        return (this.positionX == x && this.positionY == y);
    }
}

class CarteAction extends Carte {

    //ATTRIBUTS

    private int niv;

    //CONSTRUCTEUR

    public CarteAction(int niv, String name, String location) {
        super(name, location);
        this.name = name;
        this.niv = niv;
    }

    //ACCESSEUR

    public int getNiveau() {
        return this.niv;
    }

    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

            //METHODES
}
