/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.Location;

import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author greg
 */
public class Chemin {
    /*#ATTRIBUTS*/

    private CarteVille carteVilleDep;
    private CarteVille carteVilleArr;
    private boolean fini;

    private ArrayList<CarteRail> lesRails;

    /*CONSTRUCTEUR*/
    public Chemin(CarteVille villedep, CarteRail unRail) {
        this.lesRails = new ArrayList<CarteRail>();
        this.lesRails.add(unRail);
        this.carteVilleDep = villedep;
        this.fini = false;
    }

    public Chemin(CarteVille villedep, CarteVille villeArrive, CarteRail unRail) {
        this.lesRails = new ArrayList<CarteRail>();
        this.lesRails.add(unRail);
        this.carteVilleDep = villedep;
        this.carteVilleArr = villeArrive;
        this.fini = true;
    }

    /*ACCESSEUR*/
    public void finished() {
        this.fini = true;
    }

    public boolean getFinished() {
        return this.fini;
    }
    /*METHODE*/

    public void setVilleArrivee(CarteVille villeArrive) {
        this.carteVilleArr = villeArrive;
    }

    public void addCarteRail(CarteRail unRail) {
        this.lesRails.add(unRail);
    }
    
    public void dessinerCarteChemin(Scene scene, Group root){
        for(int i = 0; i<lesRails.size(); i++){
         ImageView image = new ImageView(new Image(this.lesRails.get(i).getLocationChemin()));
         image.setFitHeight(100);
         image.setFitWidth(100);
         image.setTranslateX(this.lesRails.get(i).getPositionXmap());
         image.setTranslateY(this.lesRails.get(i).getPositionYmap());
         root.getChildren().add(image);
        }
    }

}
