/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.Location;

import java.util.ArrayList;
import java.util.LinkedList;
import javafx.scene.paint.Color;
import projet_java_gl_reseau_partiereseau.joueur.Joueur;

/**
 *
 * @author greg
 */
public class Map {

    private LinkedList<Joueur> lesJoueur;//les joueur présent sur le serveur

    private String nameMap;
    private LinkedList<CarteRail> lesCartesRails;
    private LinkedList<CarteRail> lesCartesRailsInMAP;
    private LinkedList<CarteMap> lesCartesMaps;
    private ArrayList<CarteVille> lesVilles;
    private ArrayList<Chemin> lesChemins;
    private ArrayList<Marchandise> lesMarchandises;
    private ArrayList<Marchandise> lesMarchandisesReserves;
    private String[][] map;
    private final String[][] mapDebut; //la map de debut qui permet de recuperer les objet present a la base 
    private int[][] mapRail;
    //private LinkedList<CarteVille> lesCartesVilles;

    //CONSTRUCTEUR
    public Map(String nameMap, Joueur unJoueur, String map) {
        this.nameMap = nameMap;
        this.map = convertToStringArray(map);
        this.mapDebut = convertToStringArray(map);
        this.lesMarchandises = new ArrayList<Marchandise>();
        this.lesVilles = new ArrayList<CarteVille>();
        this.lesMarchandisesReserves = new ArrayList<Marchandise>();
    }

    public Map(String nameMap, String map) {
        this.nameMap = nameMap;
        this.map = convertToStringArray(map);
        this.mapDebut = convertToStringArray(map);
        this.lesMarchandises = new ArrayList<Marchandise>();
        this.lesVilles = new ArrayList<CarteVille>();
        this.lesMarchandisesReserves = new ArrayList<Marchandise>();
    }

    //ACCESSEUR
    public void setLesjoueur(LinkedList<Joueur> lesJoueur) {
        this.lesJoueur = new LinkedList<Joueur>();
        this.lesJoueur.addAll(lesJoueur);
    }

    public ArrayList<Marchandise> getMarchandises() {
        return this.lesMarchandises;
    }
    
    public ArrayList<Marchandise> getMarchandisesReserves() {
        return this.lesMarchandisesReserves;
    }

    //METHODES
    public void initMap() {
        //permet l'initialisation de la carte
        ArrayList<Color> listeCouleur = new ArrayList<Color>();//liste des couleur pour la construction de smarchandise
        listeCouleur.add(Color.RED);
        listeCouleur.add(Color.BLACK);
        listeCouleur.add(Color.YELLOW);
        listeCouleur.add(Color.BLUE);
        listeCouleur.add(Color.GREEN);

        //cration des ville et marchandise
        String name = "", location = "";
        int niveau = 0;
        for (int i = 0; i < this.map.length; i++) {
            for (int a = 0; a < this.map[i].length; a++) {
                boolean villeValide = false;
                if (this.map[i][a].equalsIgnoreCase("VM2")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_violet_2.png";
                    name = "ville";
                    niveau = 2;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VM3")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_violet_3.png";
                    name = "ville";
                    niveau = 3;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VJ2")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_jaune_2.png";
                    name = "ville";
                    niveau = 2;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VJ3")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_jaune_3.png";
                    name = "ville";
                    niveau = 3;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VB2")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_bleue_2.png";
                    name = "ville";
                    niveau = 2;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VB3")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_bleue_3.png";
                    name = "ville";
                    niveau = 3;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VG2")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_gris_2.png";
                    name = "ville";
                    niveau = 2;
                    villeValide = true;
                } else if (this.map[i][a].equalsIgnoreCase("VG3")) {
                    location = "file:/Users/greg/Desktop/projet_jeu/partie_plateau_ville_gris_3.png";
                    name = "ville";
                    niveau = 3;
                    villeValide = true;
                    //ajouter les cube a la ville suivant sont niveau 
                }
                if (villeValide) {
                    CarteVille ville = new CarteVille(name, location, niveau, a, i);
                    this.lesVilles.add(ville);
                    this.createMarchandise(listeCouleur, ville);
                }
            }
        }
    }

    //convertion map string to int[]
    private String[][] convertToStringArray(String values) {

        String[] tab = values.split(";");
        String[] tab2 = tab[0].split(",");
        String[][] tabString = new String[tab.length][tab2.length];
        for (int i = 0; i < tab.length; i++) {
            tab2 = tab[i].split(",");
            for (int a = 0; a < tab2.length; a++) {
                tabString[i][a] = tab2[a];
            }
        }
        return tabString;
    }

    //permet de crer les différente marchandise
    private void createMarchandise(ArrayList<Color> lesCouelur, CarteVille ville) {
        for (int i = 0; i < ville.getNiveau(); i++) {
            //valuer aleatoir pour la couleur des marchandise
            int index = (int) (Math.random() * lesCouelur.size());
            //creation des marhandise
            Marchandise marchandise = new Marchandise(lesCouelur.get(index), ville.getPositionXMap(), ville.getPositionYMap());
            //insertion des marchandise sur chaque ville
            marchandise.setCarte(ville);
            this.lesMarchandises.add(marchandise);
        }
    }

    private Marchandise createMarchandise(ArrayList<Color> lesCouelur) {
        //valuer aleatoir pour la couleur des marchandise
        int index = (int) (Math.random() * lesCouelur.size());
        //creation des marhandise
        return (new Marchandise(lesCouelur.get(index)));
        
    }

    //permet d'initialiser les marchandises reserve
    public void initMarchandiseReserve() {
        //permet l'initialisation de la carte
        ArrayList<Color> listeCouleur = new ArrayList<Color>();//liste des couleur pour la construction de smarchandise
        listeCouleur.add(Color.RED);
        listeCouleur.add(Color.BLACK);
        listeCouleur.add(Color.YELLOW);
        listeCouleur.add(Color.BLUE);
        listeCouleur.add(Color.GREEN);

        for (int i = 0; i < 12; i++) {
            this.lesMarchandisesReserves.add(this.createMarchandise(listeCouleur));
        }
    }
}
