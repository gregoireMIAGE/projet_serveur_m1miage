/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.Location;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author greg
 */
public class Marchandise {

    //ATRIVUTS
    private Color couleur;
    private double xMap;
    private double yMap;
    private double width;
    private double height;
    private Carte laCarte;

    //CONSTRUCTEUR
    public Marchandise(Color couleur) {
        this.couleur = couleur;
    }

    public Marchandise(Color couleur, double xMap, double yMap) {
        this.couleur = couleur;
        this.xMap = xMap;
        this.yMap = yMap;
    }

    public Marchandise(Color couleur, double xMap, double yMap, double width, double height) {
        this.couleur = couleur;
        this.xMap = xMap;
        this.yMap = yMap;
        this.width = width;
        this.height = height;
    }

    //ACCESSEUR
    public void setCarte(Carte laCarte) {
        this.laCarte = laCarte;
    }

    public Carte getCrate(Carte laCarte) {
        return this.laCarte;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setXmap(double xMap) {
        this.xMap = xMap;
    }

    public void setYmap(double yXmap) {
        this.yMap = yXmap;
    }

    //METHODES
    //envoie du message type exemple : couleur,xMap,yMap
    public String toStringMarchandise() {
        if (this.laCarte != null) {//si marchandise de ville
            return this.couleur + "," + ((CarteVille) this.laCarte).positionXmap + "," + ((CarteVille) this.laCarte).positionYmap;
        } else {//si non marchandise de reserve
            return this.couleur.toString();
        }
    }

    public boolean isEqualPosition(double x, double y) {
        return (this.xMap == x && this.yMap == y);
    }
}
