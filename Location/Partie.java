/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.Location;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.paint.Color;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import projet_java_gl_reseau_partiereseau.XML.LectureXML;
import projet_java_gl_reseau_partiereseau.joueur.Joueur;

/**
 *
 * @author greg
 */
public class Partie {

    //ATTRIBUTS
    private Map map;
    private final LinkedList<CarteAction> lesCartesActions;
    private ArrayList<CarteAction> lesCartesActionsDstrib;
    private LinkedList<Joueur> lesJoueurs;
    private ArrayList<Joueur> lesJoueursOrdreTour;
    private HashMap<Integer, Integer> ordreTour;

    //liste des couleur de chaque joueur
    private ArrayList<Color> lesCouleur;

    //CONSTRUCTEURS
    public Partie() {
        this.lesJoueurs = new LinkedList<Joueur>();
        this.lesJoueursOrdreTour = new ArrayList<Joueur>();
        this.ordreTour = new HashMap<Integer, Integer>();
        this.lesCartesActions = new LinkedList<CarteAction>();
        this.lesCartesActionsDstrib = new ArrayList<CarteAction>();
        this.lesCouleur = new ArrayList<Color>();
        this.lesCouleur.add(Color.RED);
        this.lesCouleur.add(Color.YELLOW);
        this.lesCouleur.add(Color.BLUE);
        this.lesCouleur.add(Color.GREEN);
        //recuperation des tuiles actions
        initCarteAction();
    }

    //permet de recuperer les carte action
    private void initCarteAction() {
        //permet de recuoerer les cartes actions
        decryptXmlCartesActions(LectureXML.lireXmlCartesActions());
    }

    //ACCESSEURS
    public int getNbJoueur() {
        return this.lesJoueurs.size();
    }

    //METHODES¨
    //permet de recuperer toutes les carteActions
    private void decryptXmlCartesActions(Element racine) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        NodeList cartesActionss = racine.getChildNodes();
        for (int i = 0; i < cartesActionss.getLength(); i++) {
            Element carte = (Element) cartesActionss.item(i).getChildNodes();
            String name = carte.getElementsByTagName("name").item(0).getTextContent();
            String location = carte.getElementsByTagName("location").item(0).getTextContent();
            int niveaux = Integer.parseInt(carte.getElementsByTagName("niveau").item(0).getTextContent());

            //insertion des carte actions
            this.lesCartesActions.add(new CarteAction(niveaux, name, location));
        }
    }

    //permet d'ajouter un nouveau joueur
    //permet d'eviter a plusieur Thread d'acceder au même moment a cette methode
    public synchronized void addJoueur(Joueur unJoueur) {
        //attribution d'une couleur au hasard
        int random = (int) (Math.random() * (this.lesCouleur.size()));
        unJoueur.setCouleur(this.lesCouleur.get(random));
        this.lesCouleur.remove(random);
        //ajoute le joueur a la partie
        this.lesJoueurs.add(unJoueur);
        //envoie les informations de tous les joueurs a tous les joueurs
        Thread envoieInfoJoueur = new Thread(new Runnable() {

            @Override
            public void run() {
                //permet d'envoie les infor de chaque joueur de la partie a tous les joeueur de la partie
                for (int i = 0; i < lesJoueurs.size(); i++) {
                    lesJoueurs.get(i).sendInfoJoueur(lesJoueurs);
                }
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });
        envoieInfoJoueur.start();
    }

    //permet de tester si on peux ou non demmarer la partie
    public void demarePartie() {
        //si la partie contient 4 joueur, alors on demare la partie
        if (this.lesJoueurs.size() == projet_java_gl_reseau_partiereseau.Projet_java_gl_reseau_partieReseau.nbJoueurMax) {
            pahse1();
        }
    }

    public void pahse1() {
        //envoie de la phase 1 au joueur de al partie
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendMessage("Phase:phase1");
        }
        //recuperation de la carte
        String map = LectureXML.lireXmlStringMap("/src/projet_java_gl_reseau_partiereseau/config/map.xml", "map");
        String mapRail = LectureXML.lireXmlStringMap("/src/projet_java_gl_reseau_partiereseau/config/map.xml", "mapRail");
        //initialisation de la map
        this.map = new Map("Etat-Unis", map);
        this.map.initMap();
        //initialisation des marchandise reserve
        this.map.initMarchandiseReserve();
        //ajout des joueurs a la map
        this.map.setLesjoueur(lesJoueurs);
        //dessin de la map chez les client
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendMessage("DessinMap:" + map + "/" + mapRail);//envoie de la carte au joueur
        }
        //...

        //Mise en place des marchandise
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            for (int a = 0; a < this.map.getMarchandises().size(); a++) {
                this.lesJoueurs.get(i).sendMessage("Marchandise:" + this.map.getMarchandises().get(a).toStringMarchandise());
            }
        }
        //mise en place des marchandise reserve
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            for (int a = 0; a < this.map.getMarchandisesReserves().size(); a++) {
                this.lesJoueurs.get(i).sendMessage("MarchandiseReserve:" + this.map.getMarchandisesReserves().get(a).toStringMarchandise()+",25,25");
            }
        }
        //choix des tuiles d'action et definition de l'ordre du tour
        this.distributeCarteAction();
        //on envoie les carte action definie prédédament et l'ordre de tour de chaque joueur
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendInfoCarteACtion();
            //this.lesJoueursOrdreTour.get(i).sendMessage("ordre: " + i);
        }

        //permet de dessiner les marchandise chez le client
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendMessage("DessinMarchandise");
        }
        //...

        //permet de dessiner les marchandise reserverve chez le client
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendMessage("DessinMarchandisesReserves");
        }
        //...

        //permet de mettre tous les client attente
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).attente();
        }
        //...

        //permet de mettre tous les client attente
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendInformation("Affiche:initialisation de la map...");
        }
        //...

        //tans que tous les joeuur n'on pas fini la phase on ne passe pas a la suivante
        phase2();
    }

    public void phase2() {
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendMessage("Phase:phase2");
        }
        ArrayList<Joueur> lesJoueurLocal = new ArrayList<Joueur>(this.lesJoueursOrdreTour);
        //phase de construction, le jouer qui a la carte construction commence
        int i = 0;
        while (lesJoueurLocal.size() != 0) {
            if (i == 0 && this.ordreTour.get(4) != null) {
                //faire jouer le joueur ayant la bonne carte
                //this.lesJoueurs.get(this.ordreTour.get(4)).joue();//4 est la carte construction 
                this.joueTour(this.lesJoueurs.get(this.ordreTour.get(4)));
                lesJoueurLocal.remove(this.lesJoueurs.get(this.ordreTour.get(4)));//une fois son tour fini on supprimer le joueur de la liste 
                i++;
            } else {
                //si non le tour est joué dans l'ordre
                //this.lesJoueurs.get(i).joue();//4 est la carte construction 
                this.joueTour(lesJoueurLocal.get(0));
                lesJoueurLocal.remove(0);//une fois son tour fini on supprimer le joueur de la liste 
            }
        }
        //une foie terminé on passe a la phase 3
        this.pahse3();
    }

    public void pahse3() {
                //envoie infophase3
        /*for (int i = 0; i < this.lesJoueurs.size(); i++) {
         this.lesJoueurs.get(i).sendMessage("Phase:phase2");
         }
         //phase de construction, le jouer qui a la carte construction commence
         for (int i = 0; i < this.lesJoueurs.size(); i++) {
         if (i == 0) {
         //faire jouer le joueur ayant la bonne carte
         //this.lesJoueurs.get(this.ordreTour.get(4)).joue();//4 est la carte construction 
         this.joueTour(this.ordreTour.get(2));
         } else {
         //si non le tour est joué dans l'ordre
         //this.lesJoueurs.get(i).joue();//4 est la carte construction 
         this.joueTour(i);
         }
         }
         //une foie terminé on passe a la phase4
         this.pahse4();*/
    }

    public void pahse4() {

    }

    public void pahse5() {

    }

    public void pahse6() {

    }

    //permet de distribuer aleatoirement les carte
    public void distributeCarteAction() {
        //met a 0 la list de carte a distribuer et l'aodre de tour
        this.lesCartesActionsDstrib.clear();
        this.ordreTour.clear();

        //on ajoute toutes les tuiles action
        this.lesCartesActionsDstrib.addAll(this.lesCartesActions);
        //distribution des cartes dans alléatoirement et definie l'ordre de tour
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            int random = (int) (Math.random() * (this.lesCartesActionsDstrib.size()));
            this.lesJoueurs.get(i).setCarteAction(this.lesCartesActionsDstrib.get(random));
            this.ordreTour.put(this.lesCartesActionsDstrib.get(random).getNiveau(), i);
            this.lesCartesActionsDstrib.remove(random);
        }
        //on ajoute les joueur dans l'ordre
        int i = 0;
        while (this.lesJoueursOrdreTour.size() != this.lesJoueurs.size()) {
            if (this.ordreTour.get(i) != null) {
                this.lesJoueursOrdreTour.add(this.lesJoueurs.get(this.ordreTour.get(i)));
            }
            i++;
        }
    }

    //permet de faire jouer le joueur
    private void joueTour(int index) {
        //on demande au joueur concerné de jouer
        lesJoueurs.get(index).joue();
        sendJoueur("EnCoursJeux:" + lesJoueurs.get(index).getName(), lesJoueurs.get(index));
        String message = "";
        while ((message = this.lesJoueurs.get(index).getMessage()).equalsIgnoreCase("fin") == false) {//tans qu ele joueur an pas fini de jouer on continue
            //envoiyer le message aux différent joueur
            final String envoie = message;
            sendJoueur(message, lesJoueurs.get(index));

        }
    }

    //permet de faire jouer le joueur

    private void joueTour(Joueur unJoueur) {
        //on demande au joueur concerné de jouer
        unJoueur.joue();
        sendJoueur("EnCoursJeux:" + unJoueur.getName(), unJoueur);
        String message = "";
        while ((message = unJoueur.getMessage()).equalsIgnoreCase("fin") == false) {//tans qu ele joueur an pas fini de jouer on continue
            //envoiyer le message aux différent joueur
            final String envoie = message;
            sendJoueur(message, unJoueur);
        }
    }

    //envoie d'un message au différent joueur
    private void sendJoueur(String message) {
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            this.lesJoueurs.get(i).sendMessage(message);
        }
    }

    //envoie d'un message d'un joueur au différent joueur
    private void sendJoueur(String message, Joueur unJoueur) {
        for (int i = 0; i < this.lesJoueurs.size(); i++) {
            if (unJoueur != this.lesJoueurs.get(i)) {
                this.lesJoueurs.get(i).sendMessage(message);
            }
        }
    }

    //permet d'envoyer les marchandise de la carte aux joueurs
    //envoie du message type exemple : couleur,xMap,yMap
    private void sendMarchandiseJoueur() {
        if (this.map.getMarchandises().size() > 0) {
            String message = this.map.getMarchandises().get(0).toStringMarchandise();
            for (int i = 0; i < this.map.getMarchandises().size(); i++) {
                message = message + ";" + this.map.getMarchandises().get(i).toStringMarchandise();
            }
            //envoie des marchandise aux joueur 
            this.sendJoueur(message);
        }
    }

}
