/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import projet_java_gl_reseau_partiereseau.Location.Partie;
import java.util.ArrayList;
import projet_java_gl_reseau_partiereseau.Reseaux.Serveur;

/**
 *
 * @author greg
 */
public class Projet_java_gl_reseau_partieReseau {

    /**
     * @param args the command line arguments
     */
    private static ArrayList<Partie> lesParties; //les partie en jeux 
    public static Partie partieEncours; //partie en cours de construction tans qu'il n'y a pas 4 joueurs
    public static int nbJoueurMax = 2;
    
    private static void JouerPartie() {

    }

    private static int nbClient;

    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        lesParties = new ArrayList<Partie>();
        byte buffer[] = new byte[1024];
        /*
        ServerSocket socketserver;
        

        
        
        nbClient = 0;
        int port = 2009;
        try {
            System.out.println("Demarage du serveur !");
            socketserver = new ServerSocket(port);
            while (true) {
                new Thread(new Serveur(socketserver.accept())).start();
                nbClient = nbClient + 1;
                System.out.println("clients connectés : " + nbClient);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }*/
        DatagramSocket socket = new DatagramSocket(2010);
        String donnees = "";
        String message = "";
        int taille = 0;

        System.out.println("Lancement du serveur");
        while (true) {
          DatagramPacket paquet = new DatagramPacket(buffer, buffer.length);
          DatagramPacket envoi = null;
          socket.receive(paquet);

          System.out.println("\n"+paquet.getAddress());
          taille = paquet.getLength();
          donnees = new String(paquet.getData(),0, taille);
          System.out.println("Donnees reçues = "+donnees);

          message = "Bonjour "+donnees;
          System.out.println("Donnees envoyees = "+message);
          envoi = new DatagramPacket(message.getBytes(), 
          message.length(), paquet.getAddress(), paquet.getPort());
          socket.send(envoi);
        }
    }

    public static void DemarePartie(){
        //permet de demarrer la partie
        new Thread(new Runnable(){

            @Override
            public void run() {
                partieEncours.demarePartie();
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
            
        }).start();
    }
}
