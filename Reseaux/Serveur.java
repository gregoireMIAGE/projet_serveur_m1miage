/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.Reseaux;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import projet_java_gl_reseau_partiereseau.BDD.AccesBdd;
import projet_java_gl_reseau_partiereseau.Location.Partie;
import projet_java_gl_reseau_partiereseau.Projet_java_gl_reseau_partieReseau;
import projet_java_gl_reseau_partiereseau.joueur.Joueur;

/**
 *
 * @author greg
 */
public class Serveur implements Runnable {

    //private ServerSocket socketserver;
    //private Socket socket;
    //private BufferedReader in;
    //private PrintWriter out;
    
    private int port = 2010;
    private DatagramSocket socket;
    private int taille = 1024;
    private byte buffer[] = new byte[taille];

    public Serveur() throws SocketException {
        DatagramSocket socket = new DatagramSocket(2010);
        this.socket = socket;
    }

    private boolean connexionBDD() throws IOException {
        Boolean connexion = false;
        try {
            //recuperation du user et mp
            //String message_distant = in.readLine();
            DatagramPacket message_distant = new DatagramPacket(buffer, buffer.length);
            socket.receive(message_distant);
            //connexion a la base de données
            String donnees = new String(message_distant.getData(),0, taille);
            String user = donnees.split(";")[0];
            String pass = donnees.split(";")[1];
            //
           connexion = AccesBdd.connectJoeur(user, pass);
           Boolean connect = AccesBdd.connectJoeur(user, pass);

            //on ajoute le joueur a la partie si la connexion est ok
            if (connexion) {
                //on créé un nouveau joueur
                Joueur joueur = new Joueur(user);

                if (Projet_java_gl_reseau_partieReseau.partieEncours == null || Projet_java_gl_reseau_partieReseau.partieEncours.getNbJoueur() >= Projet_java_gl_reseau_partieReseau.nbJoueurMax) {
                    //on créer une partie et ajoute un joueur
                    Projet_java_gl_reseau_partieReseau.partieEncours = new Partie();
                    Projet_java_gl_reseau_partieReseau.partieEncours.addJoueur(joueur);
                } else {
                    //on ajoute un joueur
                    Projet_java_gl_reseau_partieReseau.partieEncours.addJoueur(joueur);
                }
            }
            //out.println(connect.toString());
            //out.flush();
        } catch (IOException ex) {
            Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return connexion;
    }

    @Override
    public void run() {
        try {
            /*try {
            //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            //BufferedReader in;
            //on commence par demander une connexion
            
            //in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            //out = new PrintWriter(socket.getOutputStream());
            } catch (IOException ex) {
            Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
            }*/
            if (connexionBDD() == false) {
                System.out.println("test de connexion fail... ");
            } else {
                System.out.println("test de connexion successfull... ");
            }
            
            //on passe a la suite
        } catch (IOException ex) {
            Logger.getLogger(Serveur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
