/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.XML;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author greg
 */
public class LectureXML {

    private static File path;

    public static String lireXmlMap(String location, String id) {
        String name = "";
        path = new File(System.getProperty("user.dir" )+location);
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document dom;
            dom = builder.parse(new FileInputStream(path));
            org.w3c.dom.Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName(id);
            Node item = items.item(0).getFirstChild();
            if (item != null) {
                name = item.getNodeValue();
            } else {
                name = "";
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return name;
    }

    public static String lireXmlStringMap(String location, String id) {
        String name = "";
        path = new File(System.getProperty("user.dir" )+location);
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            org.w3c.dom.Document dom;
            dom = builder.parse(new FileInputStream(path));
            org.w3c.dom.Element root = dom.getDocumentElement();
            NodeList items = root.getElementsByTagName(id);
            Node item = items.item(0).getFirstChild();
            if (item != null) {
                name = item.getNodeValue();
            } else {
                name = "";
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return name;
    }
    
    public static Element lireXmlCartesActions() {
        return LireElementXml("/src/projet_java_gl_reseau_partiereseau/config/cartesActions.xml");
    }
    
     private static Element LireElementXml(String location) {
        String name = "";
        Element root = null;
        path = new File(System.getProperty("user.dir" )+location);
        try {
            DocumentBuilderFactory factory;
            factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            Document dom;
            dom = builder.parse(new FileInputStream(path));
            root = dom.getDocumentElement();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        } catch (org.xml.sax.SAXException e) {
            //Logger.getLogger(LectureXML.class.getName()).log(Level.SEVERE, null, ex);
            e.printStackTrace();
        }
        return root;
    }
}
