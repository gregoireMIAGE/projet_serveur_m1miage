/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.joueur;

/**
 *
 * @author greg
 */
public class Compte {
    /*ATTRIBUTS*/
    private int solde;
    private int enAttente;
    
    /*CONSTRUCTEUR*/
    public Compte(){
        this.solde = 0;
        this.enAttente = 0;
    }
    
    /*ACCESSEUR*/
    public int getSolde(){
        return this.solde;
    }
    public void addTransaction(int money){
        this.enAttente = this.enAttente + money;
    }
     /*METHODES*/
    public void calcul(){
        this.solde = this.solde + this.enAttente;
        //verifier le solde si il est sup ou inf 
        //si inf faire une demande de prés
    }
    public void initCompte(){
        this.solde = 0;
        this.enAttente = 0;
    }
    
}
