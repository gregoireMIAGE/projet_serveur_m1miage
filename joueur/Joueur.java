/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projet_java_gl_reseau_partiereseau.joueur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import projet_java_gl_reseau_partiereseau.Location.Chemin;
import projet_java_gl_reseau_partiereseau.Location.Carte;

/**
 *
 * @author greg
 */
public class Joueur {

    /*ATTRIBUT*/
    //socket client
    //private Socket socket;
    //private BufferedReader in;
    //private PrintWriter out;

    private String name;
    private Color couleur;
    private ArrayList<Chemin> lesChemins;
    private Compte unCompte;
    private int nivLocomotive;
    private int pointVictoire;
    private Carte laCarteAction;

    //attribut de dessins
    //cercle
    private Circle NivLocomotive;
    private Circle Compte;
    private Circle NbPoint;
    private ImageView nbPoint;
    //texte
    private Text nbCompteText;

    /*METHODE*/
    public Joueur(String name, Color couleur, Compte unCompte) {
        this.name = name;
        this.couleur = couleur;
        this.unCompte = unCompte;
        this.nivLocomotive = 0;
        this.pointVictoire = 0;
    }
    
    public Joueur(String name) {
        this.name = name;
        this.unCompte = new Compte();
        this.nivLocomotive = 0;
        this.pointVictoire = 0;
    
    }

    public Joueur(String name, Color couleur, Compte unCompte, int nivLocomotive, int pointVictoire) {
        this.name = name;
        this.couleur = couleur;
        this.unCompte = unCompte;
        this.nivLocomotive = nivLocomotive;
        this.pointVictoire = pointVictoire;
    }

    /*ACCESSEUR*/
    public void setCarteAction(Carte laCarte) {
        this.laCarteAction = laCarte;
    }

    public void setCouleur(Color color) {
        this.couleur = color;
    }

    public Color getCouleur() {
        return this.couleur;
    }

    public int getSolde() {
        return this.unCompte.getSolde();
    }

    public String getName() {
        return this.name;
    }

    public void addChemin(Chemin unChemin) {
        this.lesChemins.add(unChemin);
    }

    public void replaceChemin(Chemin newChemin, Chemin ancienChemin) {
        this.lesChemins.remove(ancienChemin);
        this.lesChemins.add(newChemin);
    }

    public int getNivLocomotive() {
        return this.nivLocomotive;
    }

    public int getPointVictoire() {
        return this.pointVictoire;
    }

    public void addNivLocomotive() {
        this.nivLocomotive = this.nivLocomotive + 1;
    }

    public void addPointVictoire(int pointVictoire) {
        this.pointVictoire = this.pointVictoire + pointVictoire;
    }

    /*METHODE*/
    //permet de desciner le chemin du joueur
    public void dessineChemin(Scene scene, Group root) {

    }

    public void dessineInformation(Scene scene, Group root) {
        //dessin et texte du nombre 3 
        /*this.NbPoint = new Circle(scene.getWidth() - 300, scene.getHeight() - 50, 25);
         this.NbPoint.setFill(Color.GREEN);
         root.getChildren().add(this.NbPoint);*/

        this.nbPoint = new ImageView(new Image("file:/Users/greg/Desktop/projet_jeu/nb_niveau.png"));
        this.nbPoint.setFitWidth(100);
        this.nbPoint.setFitHeight(50);
        this.nbPoint.setTranslateX(scene.getWidth() - 500);
        this.nbPoint.setTranslateY(scene.getHeight() - 70);
        root.getChildren().add(this.nbPoint);

        this.nbCompteText = new Text(this.nbPoint.getTranslateX() + (this.nbPoint.getFitWidth() / 2), this.nbPoint.getTranslateY() + (this.nbPoint.getFitHeight() / 2), String.valueOf(this.pointVictoire));
        this.nbCompteText.setFont(Font.font("Verdana", 20));
        root.getChildren().add(this.nbCompteText);

        //dessin du niveau de locomotive
    }

    public void dessineChemins(Scene scene, Group root) {
        for (int i = 0; i < this.lesChemins.size(); i++) {
            this.lesChemins.get(i).dessinerCarteChemin(scene, root);
        }
    }

    //permet l'initialisation de son compte
    public void initCompte() {
        if (this.unCompte == null) {
            this.unCompte = new Compte();
        }
        this.unCompte.initCompte();
    }

    //pemret d'envoyer la demande jouer au joueur
    public void joue() {
        this.sendMessage("Joue");
    }

    //le joueur se trouve en attente et reçoi toute les info du joueur qui joue
    public void attente() {
        //le joueur se trouve en attente et reçoi toute les info du joueur qui joue
        this.sendMessage("Attente");
    }

    //permet d'afficher une information aux joueur
    public void sendInformation(String message) {
        //le joueur se trouve en attente et reçoi toute les info du joueur qui joue
        this.sendMessage(message);
    }

    //permet d'envoyer un message au joiueur
    public void sendMessage(String message){
        int port = 2010;
        int taille = 1024;
        byte buffer[] = new byte[taille];
        
        try {
            InetAddress serveur = InetAddress.getLocalHost();
            
            //int length = argv[1].length();
            //byte buffer[] = argv[1].getBytes();
            DatagramSocket socket = new DatagramSocket();
            buffer=message.getBytes();
            DatagramPacket donneesEmises = new DatagramPacket(buffer, buffer.length, serveur, port);
       
            socket.send(donneesEmises);
 
        } catch (SocketTimeoutException ste) {
            System.out.println("Le delai pour la reponse a expire");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //permet d'envoyer les infor de chaque joueur aux joueur
    public void sendInfoJoueur(LinkedList<Joueur> lesJoueur) {
        String message = "player:" + lesJoueur.get(0).getName() + "," + lesJoueur.get(0).getCouleur().toString() + "," + lesJoueur.get(0).getSolde();
        for (int i = 1; i < lesJoueur.size(); i++) {
            message = message + ";" + lesJoueur.get(i).getName() + "," + lesJoueur.get(i).getCouleur().toString() + "," + lesJoueur.get(i).getSolde();
        }
        this.sendMessage(message);
    }

    //permet d'envoie la carte action au joueur
    public void sendInfoCarteACtion() {
        sendMessage("CarteAction:" + this.laCarteAction.getName() + "," + this.laCarteAction.getLocation());
    }
}
